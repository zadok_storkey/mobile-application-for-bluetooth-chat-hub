﻿using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using ChatApp.Droid;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

// Code comes from stackoverflow user djunod at https://stackoverflow.com/a/46065804 and therefore is covered by the Creative Commons Attribution-ShareAlike 2.5 Generic Liscence (https://creativecommons.org/licenses/by-sa/2.5/)

[assembly: ExportRenderer(typeof(Xamarin.Forms.Entry), typeof(ChatEntryRenderer))]
namespace ChatApp.Droid
{
    public class ChatEntryRenderer : EntryRenderer
    {
        public ChatEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null && e.NewElement != null)
            {
                var entry = (Xamarin.Forms.Entry)e.NewElement;
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(entry.TextColor.ToAndroid());
                else
                    Control.Background.SetColorFilter(entry.TextColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "TextColor")
            {
                var entry = (Xamarin.Forms.Entry)sender;
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(entry.TextColor.ToAndroid());
                else
                    Control.Background.SetColorFilter(entry.TextColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }
        }
    }
}