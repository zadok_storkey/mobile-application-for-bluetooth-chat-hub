﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ChatApp.Model
{
    public class MessageHolder : ObservableObject
    {
        private ObservableCollection<Message> _messages = new ObservableCollection<Message> { new Message("Hi", Color.White, Color.DodgerBlue, true, DateTime.Now.AddMinutes(-20)), new Message("Hello", Color.Black, Color.LightGray, false, DateTime.Now.AddSeconds(-30))};

        public ObservableCollection<Message> Messages
        {
            get
            {
                return _messages;
            }

            set
            {
                _messages = value;
            }
        }

        public void AddMessage(Message message)
        {
            _messages.Add(message);
        }
    }
}
