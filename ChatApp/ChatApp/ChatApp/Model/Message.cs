﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ChatApp.Model
{
    public class Message
    {
        public string MessageText { get; }
        public Color TextColor { get; }
        public Color BackgroundColor { get; }
        public bool IsMyMessage { get; }

        public DateTime TimeStamp { get; }

        public Message(string messageText, Color textColor, Color backgroundColor, bool isMyMessage, DateTime timeStamp)
        {
            MessageText = messageText;
            TextColor = textColor;
            BackgroundColor = backgroundColor;
            IsMyMessage = isMyMessage;
            TimeStamp = timeStamp; 
        }
    }
}
