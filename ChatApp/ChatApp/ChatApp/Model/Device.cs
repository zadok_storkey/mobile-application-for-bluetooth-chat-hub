﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ChatApp.Model
{
    public class Device
    {
        public string DeviceName { get; }

        public Device(string deviceName)
        {
            DeviceName = deviceName;
        }
    }
}
