﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ChatApp.Services;
using ChatApp.View;
using ChatApp.IoC;
using ChatApp.View.CustomComponents;

namespace ChatApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new MessagingPage();
            MainPage = new ModifiedNavigationPage(new DeviceSelectPage());
            //MainPage = new NameAndColorSelectPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
