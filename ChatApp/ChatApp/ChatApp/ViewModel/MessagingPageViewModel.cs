﻿using ChatApp.IoC;
using ChatApp.Model;
using ChatApp.Services;
using ChatApp.View;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace ChatApp.ViewModel
{
    public class MessagingPageViewModel : ViewModelBase, IMessagingPageViewModel
    {
        private readonly IMessageService _messageService;
        private ObservableCollection<Message> _messages;
        private string _entryText = "";

        public ObservableCollection<Message> Messages {
        
            get
            {
                return _messages;
            }
        }

        public string EntryText
        {
            get
            {
                return _entryText;
            }
            set
            {
                _entryText = value;
                RaisePropertyChanged(EntryText);
            }
        }

        public MessagingPageViewModel()
        {
            _messageService = IocLocator.MessageService;
            _messageService.OnNewMessage += (object sender, EventArgs e) => UpdateMessages();
            UpdateMessages();
        }

        public void OnMessageEntryCompleted(object sender, EventArgs e)
        {
           if (_entryText != "")
            {
                _messageService.AddMessage(_entryText);
                _entryText = "";
                RaisePropertyChanged(nameof(EntryText));

                Element eventElement = sender as Element;

                Element parent = eventElement.Parent;
                while (!(parent is MessagingPage))
                {
                    parent = parent.Parent;
                }
                MessagingPage messagingPage = (MessagingPage)parent;
                messagingPage.MainCollectionView.ScrollTo(_messages.Count);
            }
        }

        public void UpdateMessages()
        {
            _messages = _messageService.Messages;
            RaisePropertyChanged(nameof(Messages));
        }

    }
}
