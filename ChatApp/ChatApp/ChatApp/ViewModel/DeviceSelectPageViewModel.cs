﻿using ChatApp.Model;
using ChatApp.View;
using ChatApp.View.CustomComponents;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ChatApp.ViewModel
{
    public class DeviceSelectPageViewModel : ViewModelBase, IDeviceSelectPageViewModel
    {
        public DeviceSelectPageViewModel()
        {

        }

        public ObservableCollection<Device> AvailableDevices
        {
            get
            {
                // The service to manage devices will be added much later so for now we will supply them here for testing purposes
                return new ObservableCollection<Device> { new Device("Device 1"), new Device("Device 2"), new Device("Device 3"), new Device("Device 4"), new Device("Device 5"), new Device("Device 6"), new Device("Device 7"), new Device("Device 8"), new Device("Device 9") };
            }
        }

        public void OnDeviceSelected(object sender, EventArgs e)
        {
            // Device connection logic here
        }

        public void OnDevicePressed(object sender, EventArgs e)
        {
            // Some testing to see if device is already connected
            ((ModifiedNavigationPage)App.Current.MainPage).SetCurrentSecondaryPage(new NameAndColorSelectPage());
        }
    }
}
