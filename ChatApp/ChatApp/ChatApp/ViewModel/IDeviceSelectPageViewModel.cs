﻿using ChatApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ChatApp.ViewModel
{
    public interface IDeviceSelectPageViewModel
    {
        ObservableCollection<Device> AvailableDevices { get; }

        void OnDeviceSelected(object sender, EventArgs e);

        void OnDevicePressed(object sender, EventArgs e);
    }
}
