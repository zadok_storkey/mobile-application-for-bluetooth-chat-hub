﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ChatApp.ViewModel
{
    public interface INameAndColorSelectPageViewModel
    {
        string UserNameEntry { get; set; }

        Color Color { get; set; }

        void OnJoinButtonPressed(object sender, EventArgs e);

        void OnColorButtonPressed(object sender, EventArgs e);

        void OnColorPopupButtonPressed(object sender, EventArgs e);
    }
}
