﻿using ChatApp.View;
using ChatApp.View.CustomComponents;
using GalaSoft.MvvmLight;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ChatApp.ViewModel
{
    public class NameAndColorSelectPageViewModel : ViewModelBase, INameAndColorSelectPageViewModel
    {
        private string _usernameEntry = "";

        private Color _color;
        
        public string UserNameEntry
        {
            get
            {
                return _usernameEntry;
            }
            set
            {
                _usernameEntry = value;
                RaisePropertyChanged(nameof(UserNameEntry));
            }
        }

        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                RaisePropertyChanged(nameof(Color));
            }
        }

        public void OnJoinButtonPressed(object sender, EventArgs e)
        {
            ((ModifiedNavigationPage)App.Current.MainPage).SetCurrentSecondaryPage(new MessagingPage());
        }

        public async void OnColorButtonPressed(object sender, EventArgs e)
        {
            ColorButton colorButton = sender as ColorButton;
            _color = colorButton.ColorToSelect;
            RaisePropertyChanged(nameof(Color));
            await PopupNavigation.Instance.PopAsync();
        }

        public async void OnColorPopupButtonPressed(object sender, EventArgs e)
        {
            Button colorPopupButton = sender as Button;

            double popupx = colorPopupButton.X;
            double popupy = colorPopupButton.Y;

            // This iterates through all the parents and adds the relative positions together to get the absolute position
            VisualElement parent = (VisualElement)colorPopupButton.Parent;
            while (parent != null)
            {
                popupx += parent.X;
                popupy += parent.Y;
                if (parent.Parent.GetType() == typeof(App))
                {
                    parent = null;
                }
                else
                {
                    parent = (VisualElement)parent.Parent;
                }
            }

            // Send the coordinates to a quarter the height above the top-center of the button rather than to the top-left of the button
            popupx += (colorPopupButton.Width / 2);
            popupy -= (colorPopupButton.Height / 4);

            ColorPickerPopupPage colorPicker = new ColorPickerPopupPage(popupx, popupy);
            await PopupNavigation.Instance.PushAsync(colorPicker);
        }
    }
}
