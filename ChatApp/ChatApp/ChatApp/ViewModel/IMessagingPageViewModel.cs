﻿using ChatApp.IoC;
using ChatApp.Model;
using ChatApp.Services;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ChatApp.ViewModel
{
    public interface IMessagingPageViewModel
    {
        ObservableCollection<Message> Messages { get; }

        string EntryText { get; set; }

        void OnMessageEntryCompleted(object sender, EventArgs e);

        void UpdateMessages();

    }
}
