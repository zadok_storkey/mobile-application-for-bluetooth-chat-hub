﻿using ChatApp.Model;
using System;
using System.Collections.ObjectModel;

namespace ChatApp.Services
{
    public interface IMessageService
    {
        ObservableCollection<Message> Messages { get; }

        event EventHandler<EventArgs> OnNewMessage;

        void AddMessage(string messageText);
    }
}