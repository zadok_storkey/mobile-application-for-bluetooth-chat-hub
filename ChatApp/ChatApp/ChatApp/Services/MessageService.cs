﻿using ChatApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace ChatApp.Services
{
    public class MessageService : IMessageService
    {
        private readonly MessageHolder _messageHolder;

        public MessageService (){
            _messageHolder = new MessageHolder();
        }

        // Create our own event to handle when the TestModel's CurrentText changes
        public event EventHandler<EventArgs> OnNewMessage;

        // Allow someone to use the service to get or set the CurrentText
        public ObservableCollection<Message> Messages
        {
            get
            {
                return _messageHolder.Messages;
            }
        }

        public void AddMessage(string messageText)
        {
            _messageHolder.AddMessage(new Message(messageText, Color.White, Color.DodgerBlue, true, DateTime.Now));
            OnNewMessage.Invoke(this, EventArgs.Empty);
        }
    }
}
