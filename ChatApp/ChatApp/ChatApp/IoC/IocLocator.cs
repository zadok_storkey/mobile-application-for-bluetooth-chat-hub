﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Text;
using ChatApp.ViewModel;
using ChatApp.Services;

namespace ChatApp.IoC
{
    /* 
     * Provides the correct instance of a viewmodel or service to anyone who requests it.
     */
    
    public static class IocLocator
    {
        static IocLocator()
        {
            SimpleIoc.Default.Register<IMessagingPageViewModel>(() => new MessagingPageViewModel());
            SimpleIoc.Default.Register<IDeviceSelectPageViewModel>(() => new DeviceSelectPageViewModel());
            SimpleIoc.Default.Register<INameAndColorSelectPageViewModel>(() => new NameAndColorSelectPageViewModel());

            SimpleIoc.Default.Register<IMessageService>(() => new MessageService());
        }

        public static IMessagingPageViewModel MessagingPageViewModel
        {
            get
            {
                if (!SimpleIoc.Default.IsRegistered<IMessagingPageViewModel> ())
                {
                    SimpleIoc.Default.Register<IMessagingPageViewModel>();
                }
                return SimpleIoc.Default.GetInstance<IMessagingPageViewModel>();
            }
        }

        public static IMessageService MessageService
        {
            get
            {
                if (!SimpleIoc.Default.IsRegistered<IMessageService>())
                {
                    SimpleIoc.Default.Register<IMessageService>();
                }
                return SimpleIoc.Default.GetInstance<IMessageService>();
            }
        }

        public static IDeviceSelectPageViewModel DeviceSelectPageViewModel
        {
            get
            {
                if (!SimpleIoc.Default.IsRegistered<IDeviceSelectPageViewModel>())
                {
                    SimpleIoc.Default.Register<IDeviceSelectPageViewModel>();
                }
                return SimpleIoc.Default.GetInstance<IDeviceSelectPageViewModel>();
            }
        }

        public static INameAndColorSelectPageViewModel NameAndColorSelectPageViewModel
        {
            get
            {
                if (!SimpleIoc.Default.IsRegistered<INameAndColorSelectPageViewModel>())
                {
                    SimpleIoc.Default.Register<INameAndColorSelectPageViewModel>();
                }
                return SimpleIoc.Default.GetInstance<INameAndColorSelectPageViewModel>();
            }
        }
    }
}