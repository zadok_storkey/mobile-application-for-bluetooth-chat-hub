﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ChatApp.Converter
{
    public class ValueMinusFiftyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int)) throw new ArgumentException("Arguement should be an integer");
            return (int)value - 50;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int)) throw new ArgumentException("Arguement should be an integer");
            return (int)value + 50;
        }
    }
}
