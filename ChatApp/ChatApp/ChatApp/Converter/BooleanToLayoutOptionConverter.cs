﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ChatApp.Converter
{
    public class BooleanToLayoutOptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) throw new ArgumentException("Arguement should be a boolean");
            return !(bool)value ? LayoutOptions.Start : LayoutOptions.End;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is LayoutOptions)) throw new ArgumentException("Arguement should be a LayoutOptions");

            if (((LayoutOptions)value).Equals(LayoutOptions.Start))
            {
                return false;
            }

            else if(((LayoutOptions)value).Equals(LayoutOptions.End))
            {
                return true;
            }

            else
            {
                throw new ArgumentException("Arguement must be Start or End");
            }
        }
    }
}
