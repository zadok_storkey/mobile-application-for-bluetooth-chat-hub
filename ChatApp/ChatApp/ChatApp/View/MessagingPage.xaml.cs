﻿using ChatApp.IoC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatApp.View
{
    [DesignTimeVisible(false)]
    public partial class MessagingPage : ContentPage
    {
        public CollectionView MainCollectionView
        {
            get
            {
                return _mainCollectionView;
            }
        }

        public MessagingPage()
        {
            InitializeComponent();
            BindingContext = IocLocator.MessagingPageViewModel;
            MessageEntry.Completed += IocLocator.MessagingPageViewModel.OnMessageEntryCompleted;
            SendButton.Pressed += IocLocator.MessagingPageViewModel.OnMessageEntryCompleted;
        }
    }
}
