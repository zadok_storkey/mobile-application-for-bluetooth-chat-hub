﻿using ChatApp.IoC;
using ChatApp.View.CustomComponents;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NameAndColorSelectPage : ContentPage
    {
        public NameAndColorSelectPage()
        {
            InitializeComponent();
            BindingContext = IocLocator.NameAndColorSelectPageViewModel;
            JoinButton.Clicked += IocLocator.NameAndColorSelectPageViewModel.OnJoinButtonPressed;
            ColorPopupButton.Clicked += IocLocator.NameAndColorSelectPageViewModel.OnColorPopupButtonPressed; // This event stays in the view because it doesn't change any data

        }
    }
}