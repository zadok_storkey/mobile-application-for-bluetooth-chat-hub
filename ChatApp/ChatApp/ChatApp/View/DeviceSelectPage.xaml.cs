﻿using ChatApp.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeviceSelectPage : ContentPage
    {
        public DeviceSelectPage()
        {
            InitializeComponent();
            BindingContext = IocLocator.DeviceSelectPageViewModel;
            AvailableDevicesListView.ItemTapped += IocLocator.DeviceSelectPageViewModel.OnDevicePressed;
            AvailableDevicesListView.ItemSelected += IocLocator.DeviceSelectPageViewModel.OnDeviceSelected;
        }
    }
}