﻿using ChatApp.IoC;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ChatApp.View.CustomComponents
{
    public class ColorButton : Button
    {
        public Color ColorToSelect {
            get
            {
                return (Color)GetValue(ColorToSelectProperty); ;
            }
            set
            {
                SetValue(ColorToSelectProperty, value);
            }
        }

        public static readonly BindableProperty ColorToSelectProperty = BindableProperty.Create(
                                                         propertyName: "ColorToSelect",
                                                         returnType: typeof(Color),
                                                         declaringType: typeof(ColorButton),
                                                         defaultValue: Color.White,
                                                         defaultBindingMode: BindingMode.TwoWay);

        public ColorButton() : base()
        {
            this.WidthRequest = 0;
            this.HeightRequest = 0;
            this.CornerRadius = 20;
            this.Pressed += IocLocator.NameAndColorSelectPageViewModel.OnColorButtonPressed;
        }
    }
}
