﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ChatApp.View.CustomComponents
{
    public class ModifiedNavigationPage : NavigationPage
    {
        public ModifiedNavigationPage(Page page) : base(page)
        {

        }

        public async void SetCurrentSecondaryPage(Page page)
        {
            await App.Current.MainPage.Navigation.PushAsync(page);
            while (App.Current.MainPage.Navigation.NavigationStack.Count > 2)
            {
                App.Current.MainPage.Navigation.RemovePage(App.Current.MainPage.Navigation.NavigationStack[1]);
            }
        }
    }
}
