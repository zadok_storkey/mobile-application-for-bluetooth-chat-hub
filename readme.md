## Mobile Application For Bluetooth Chat Hub

### General concepts
- [C#](./Docs/General%20Concepts/CSharp.md)
- [Xamarin](./Docs/General%20Concepts/Xamarin.md)
- [The MVVM approach to designing applications](./Docs/General%20Concepts/MVVM.md)
- [Inversion of control](./Docs/General%20Concepts/IoC.md)

### Contributing
- [Planning the Project Using JIRA](./Docs/Contributing/Planning%20the%20Project%20Using%20JIRA.md)
- [Making changes to the code](./Docs/Contributing/Making%20Changes%20To%20The%20Code.md)
- [Installing Visual Studio and setting up a workspace](./Docs/Contributing/Installing%20Git%20And%20Visual%20Studio%20And%20Setting%20Up%20A%20Workspace.md)
- [Testing the app](./Docs/Contributing/Testing%20The%20App.md)

### In depth documentation
- View
    - [MessagingPage](./Docs/In%20Depth/MessagingPage.md)
    - [DeviceSelectPage](./Docs/In%20Depth/DeviceSelectPage.md)
    - [NameAndColorSelectPage](./Docs/In%20Depth/NameAndColorSelectPage.md)
    - CustomComponents
        - [ColorPickerPopupPage](./Docs/In%20Depth/ColorPickerPopupPage.md)
        - [ModifiedNavigationPage](./Docs/In%20Depth/ModifiedNavigationPage.md)
        - [ColorButton](./Docs/In%20Depth/ColorButton.md)
- ViewModel
    - [MessagingPageViewModel](./Docs/In%20Depth/MessagingPageViewModel.md)
    - [DeviceSelectPageViewModel](./Docs/In%20Depth/DeviceSelectPageViewModel.md)
    - [NameAndColorSelectPageViewModel](./Docs/In%20Depth/NameAndColorSelectPageViewModel.md)
- Model
    - [Device](./Docs/In%20Depth/Device.md)
    - [Message](./Docs/In%20Depth/Message.md)
    - [MessageHolder](./Docs/In%20Depth/MessageHolder.md)
- Services
    - [MessageService](./Docs/In%20Depth/MessageService.md)
- Ioc
    - [IocLocator](./Docs/In%20Depth/IocLocator.md)
- Converters
    - [BooleanToLayoutOptionConverter](./Docs/In%20Depth/BooleanToLayoutOptionConverter.md)
    - [ValueMinusFiftyConverter](./Docs/In%20Depth/ValueMinusFiftyConverter.md)
    - [DeviceToDeviceDetailTextConverter](./Docs/In%20Depth/DeviceToDeviceDetailTextConverter.md)
- Android Renderers
    - [ChatEntryRenderer](./Docs/In%20Depth/ChatEntryRenderer.md)

### Other Links
- [Google Drive](https://drive.google.com/drive/folders/1DFmxpAP7anuxPi82YxdgkgSt-yF_IwvY?usp=sharing)
- [JIRA](https://engineeringdesigngroup6.atlassian.net/jira/software/projects/MAFBCH/boards/3)

### Documentation Versions
- Current Documentation
    - [Master](https://bitbucket.org/zadok_storkey/mobile-application-for-bluetooth-chat-hub/src/master/readme.md)
    - [Develop](https://bitbucket.org/zadok_storkey/mobile-application-for-bluetooth-chat-hub/src/develop/readme.md)
- Historic Documentation
    - [Version 0.1.0](https://bitbucket.org/zadok_storkey/mobile-application-for-bluetooth-chat-hub/src/v0.1.0/readme.md)