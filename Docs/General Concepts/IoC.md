## Inversion of Control

### What is Inversion of Control?

Inversion of control (abbreviated IoC) is the idea that when multiple classes want a single instance of a class, rather than storing the instance of the class in the class itself as a static variable, you instead store it elsewhere, in a class designed to contain all these instances.

### How does it work in practice?

If you had a service that multiple classes wanted an instance of, without inversion of control you would store it as a static variable within itself. An example would be this:

```csharp
public class ExampleService
{
    public static ExampleService ExampleServiceInstance = new ExampleService();
}
```

When a class wanted an instance of it, they would then get `ExampleService.ExampleServiceInstance`.

With inversion of control, you instead define a container containing all the instances and anyone can request it. That would look something like this:

```csharp
public static class ExampleIocContainer
{
    private static ExampleService _exampleServiceInstance;

    static ExampleIocContainer()
    {
        _exampleServiceInstance = new ExampleService();
    }

    public static ExampleService ExampleServiceInstance
    {
        get
        {
            return _exampleServiceInstance;
        }
    }
}
```

When a class needs it, they then instead look for `ExampleIocContainer.ExampleServiceInstance`

### Why is this useful?

There are a couple of ways inversion of control is useful:

- It means that all the instances are accessed in the same place
- It means that the access to the instances can be readonly

### What is SimpleIoc?

SimpleIoc is a package which does most of the work with inversion of control for you. You can call methods of SimpleIoc to register an instance of a type, get and instance of a type or check whether an instance of a type exists.
