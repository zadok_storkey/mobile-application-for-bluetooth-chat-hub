## C Sharp

The majority of the code for the app is in C\# so I thought it would be worth making a page on the language.

### General features of the language

#### Object Oriented 

C# is an object oriented language which means it mainly relys on using classes and methods to structure the code.

#### Strict typing

C# is a strictly typed language which means all data must have an explicit type and operations and functions will only work for specific types.

### Specific things you might not see in every other language

#### Generics

Generics are classes which you define some types with when you instantiate them. For example a list in C# is a generic because you can create a `List<int>` or a `List<string>` for example. An example of creating a generic class would be:

```csharp
public class Box<T>
{
    private T _itemInBox;

    public Box(T itemInBox)
    {
        _itemInBox = itemInBox;
    }

    public T GetItem()
    {
        return _itemInBox;
    }
}
```

You can see from this example that you can have a box of anything and what ever type you choose it will define the type of _itemInBox, the type of what you need to put in the constructor, and the type of what is returned by GetItem.

#### Getters and Setters

Getters and setters are special functions defining how to set a variable. There are similar things in java but in java they just use normal functions wheras in C# there is a special syntax for them. An example of a getter and setter would be this:

```csharp
public class ExampleClass
{
    private int number = 0;
    private int _count = 0;

    public int Number
    {
        get
        {
            return _number;
        }
        set
        {
            _variable = value;
            _count++;
        }
    }
}
```

This means that from outside the class the code is only able to access the variable '\number' using the public variable Number. And the class is able to do other things when you set a value. So for example someone could type

```csharp
TestClass testClass = new TestClass();
TestClass.Number = 5;
```

The value of \number would then be 5 and the value of count would be 1.



#### Lambda Functions

A lamda function is a function defined without being named. So for example a lambda function to square a number in c# in c# would look somethings like this:

```csharp
(x) => x * x
```

Here the left hand side is defining the arguements to the function and the right hand side is defining what the function returns. So this function would take the number x and return x squared. You could also define the types explicitly if you wanted:

```csharp
(int x) => x * x
```

#### Interfaces

Interfaces are definitions of what a class might look at without actually implementing those defenitions. They define what you should expect to see in a class which implements them. It helps in a situation where you have multiple classes with the same properties but don't have anything in common outside those properties. An example of use of an interface might be:

```csharp
public interface IIsAbleToFly
{
    void fly();
}

public interface Plane : Vehicle, IIsAbleToFly
{
    void fly()
    {
        //do stuff
    }
}

public interface Bird : Animal, IIsAbleToFly
{
    void fly()
    {
        //do stuff
    }
}
```

The bird and the plane are completely different things which is why they extend different classes. But they have a common feature to put in an interface which is being able to fly. This is useful because something might only want to accept things which can fly, but might not mind whether it gets a plane or a bird because it is only interested in the fly method which they both have.

#### Abstract classes

Abstract classes are classes you cannot create instances of. They are only for extending with other classes. An example of a use of an abstract class might be:

```csharp
public abstract class Shape
{
    //some shape related stuff
}

public class Square : Shape
{
    //some square related stuff
}

public class Circle : Shape
{
    //some circle related stuff
}
```

Shape is abstract because you can't create an instance of a shape, you can only create instances of classes that extend it, like circle and square.

#### Statics

Static variables are variables which are attached to a class that you only have one of. For example you may have a static method in a class which would then run the same way whatever instance you called it from. You can also have a static class which is a class that you cannot instantiate and only has statics in.

```csharp
public class ExampleClass
{
    public void NonStaticMethod()
    {
        //dostuff
    }
    public static void StaticMethod()
    {
        //dostuff
    }
}
```

With this class to use the non static method you would use
```csharp
new ExampleClass().NonStaticMethod();
```
wheras with the static method you would use
```csharp
ExampleClass.StaticMethod();
```

This is because the non static method is a method belonging to an instance of the class, whereas a static method belongs to the class itself.
