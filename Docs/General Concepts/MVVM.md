## The MVVM aproach to designing applications

### What is MVVM?

MVVM stands for Model-View-ViewModel. It is a structure used in the design of applications. The premise is that you want to seperate the part of the application responsible for the user interface (the view) from the actual data in the application (the model). You do this by seperating your application into views, models, viewmodels and services.

### What is a view?

A view is the user interface of your application. In Xamarin apps it is mostly done in XAML. It should contain all the information for how the user interface should look but not store any of the data or processes used in the UI. It is just how the data will be presented.

### What is a model?

A model is the actual data relating to the application. It doesn't actually do any processing of information it is just a definition of what all the data is. An example of something which would go in the model is the idea of a message. All it knows is what a message is and what properties it has and how to create one, but the model doesn't know how to send a message or anything like that.

### What is a view model?

A view model is responsible for handling and presenting the data which is used by the user interface. It acts as a midpoint between the model and the view. Storing data temporarily for the view but updating that data based on what the model says and passing any changes to the data to the model. The view model is also responsible converting the data in the model to something suitable to show in the view, for example by changing from a Unix Time to a time specific to the phone's timezone.

### What is a service?

A service is any process which isn't related to the user interface. A service could be used by the view model to get data from the model but a service could also be used for something completely unrelated like communication over networks.

