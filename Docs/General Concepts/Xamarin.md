## Xamarin

### The cross platform project setup

#### Project Structure

Because xamarin can build apps for multiple platforms it needs a more complicated setup. This is why there are three different projects in the solution, ChatApp, ChatApp.Android and ChatApp.iOS. There could have also been a version for windows phones it was simpler not to include.

The basic structure is that the platform specific projects both reference the generic project. All the code for the app then goes in the shared project, with the exception of some code for rendering specific user interface elements, which might be in the platform specific projects.

#### Platform Specific Compilation

Sometimes in the generic project you need to have a small part which will work differently on different platforms. For this there is a feature in C# called conditional compilation where a section will only be added to the compilation if a condition is true. This is useful because Xamarin defines some variables to use with conditional compilation to make sure code only compiles on one platform. It looks something like this:

```csharp
#if __IOS__

// iOS-specific code

#elif __ANDROID__

// Android specific code

#endif
```

It is worth mentioning that you can also target specific versions of android with `__ANDROID_[API VERSION]__`.

### Working with the XAML views

#### What is XAML?

All the code for this app that isn't in C# will be done in XAML. Xaml is a similar language to markup languages like XML and HTML. It defines individual objects by surrounding them with angle brackets. The basic structure of XAML looks something like this:

```xml
<ParentObject property=value>
    <ChildObject property=value/>
</ParentObject>
```

You can see that the parent object contains the child object and has a opening and closing tag at the start and end. The child object is opened and closed in the same tag as it doesn't have any contents. Both also can be assigned values for their properties.

#### How does Xamarin use XAML?

Xamarin uses XAML to define the layout of a view much in the same way a webpage uses HTML to define the layout. There are definitions of each components and whether the components contain each other. One significant difference between the HTML in a webage and XAML used with Xamarin is that the XAML also contains all the visual information such as the margin and padding, the position on a grid, the alignment of text, ect. rather than relying on something like css for that. This means that the entire information to display a page with the exception of the actual data to display is contained in the XAML file.

Xamarin relies on specific container objects to structure the page such as a carosel view for pages to swipe between or a list view to display a vertical scrollable list of items.

#### What elements are there?

I will go through some of the elements but for a list of them all you can go to the [Xamarin Documentation](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/controls/).

##### Pages

Pages are the high level objects which generally cover the whole screen. They can go from basic pages which display things on the screen like the ContentPage to pages with tabs where you can navigate by pressing the tabs or swiping left and right like the TabbedPage. A list of these can be found [here](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/controls/pages).

##### Layouts

Layouts are structured ways of laying out other objects. Examples are the stack layout which stacks the elements below each other in order, and the grid, which places elements by a coordinate system. A list of these can be found [here](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/controls/layouts).

##### Other Elements

There are a number of other elements but I just wanted to focus on a few common ones:

- A Label displays text
- A Button is something which can be attached to an event which fires when it is pressed
- An Image displays an image
- An ImageButton is an image which can be pressed like a button
- An Entry allows the user to fill in text

#### How do bindings work?

Bindings link properties of an element to some data usually in the viewmodel. You begin by defining a BindingContext. For example in the MainPage in this app the BindingContext is set to the MainPageViewModel so when I bind a property to something it will look for what I told it to look for in the MainPageViewModel. A simple example would be this:

```xml
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:d="http://xamarin.com/schemas/2014/forms/design"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:ioc="clr-namespace:ExampleApp.IoC.IocLocator"
             mc:Ignorable="d"
             x:Class="ExampleApp.View.ExampleTextPage"
             BindingContext="ioc:ExampleTextPageViewModel">
    <ContentView>
        <Label Text={Binding LabelText}/>
    </ContentView>
</ContentPage>
```

This would bind the Text of the label to the ExampleTextPageViewModel's LabelText property.

If you want to read about more complex bindings then you can find them [here](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/xaml/xaml-basics/data-binding-basics).
