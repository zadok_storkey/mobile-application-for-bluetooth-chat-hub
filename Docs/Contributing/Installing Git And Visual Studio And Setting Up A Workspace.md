## Installing git and visual studio and setting up a workspace

### Installing git

#### Windows

If you are on windows go to https://gitforwindows.org/ and download the installer. Follow it until you get to the first set of options. Here, the only options you probably want ticked are the 'git bash' option and 'Associate .git* configuration files with the default text editor'.

![Git Installation Settings](../Images/GitInstallSettings.PNG)

Keep going through the installer until you get to the bit where you choose the default editor. You may keep it as vim or you may change it to something else. This documentation is written assuming you are using vim though. Go through the rest of the installer leaving all the options as their defaults and install git.

A 'git' folder containing 'git bash' should now be on your list of programs on your computer.

#### Mac

If you have a mac you can download it from https://git-scm.com/download/mac (this goes straight to the download). Continue through the installer and it should install git.

### Installing Visual Studio 2019 Community Edition

If you want to be able to test and modify the app properly then you will need [visual studio 2019 community edition](https://visualstudio.microsoft.com/vs/community/ "Visual Studio Community"). Download the installer and follow the normal steps until it gets to the screen asking which parts to install. The only thing which needs to be ticked is 'Mobile Development With .NET', which is about half way down the scrollable page in the 'workloads tab'. 

![Visual Studio Installation Settings](../Images/VisualStudioInstallSettings.PNG)

Once that is complete then you have successfully installed Visual Studio in a way which can be used for app development.

### Setting everything up so that you can make changes

Open git bash (or the terminal on a Mac) and type `git config --global user.email [your email adress]` and `git config --global user.name [your name]`. . `cd [full path of the folder you want to be in]` to switch to looking at that folder. Then type `git clone git@bitbucket.org:zadok_storkey/mobile-application-for-bluetooth-chat-hub.git [what you want the folder to be called]`. This will create a duplicate of the bitbucket repository in your local folder. Then you should use `cd [directory you just made]` to enter the folder and then git checkout develop to switch to looking at the develop branch. From there you should see a file called 'ChatApp.sln'. Double click it and it should open in visual studio. You should then be able to run the project using the green arrow at the top.

![The Solution Open In Visual Studio](../Images/VisualStudioProject.PNG)

Now that you have a working copy of the project you need to be able to push changes. To do this you need an SSH key. Type `ssh-keygen` and it will ask you to enter a file to save the key into. You probably just want to press the enter key to select the default location. It will then ask for a passphrase, which you can leave empty.

Once you have done that it should have saved two files into the directory it mentioned before: 'id\_rsa' and 'id\_rsa.pub'. Finally, you need to go to bitbucket and under your account settings in the security section you should find a section entitled 'SSH keys'. Press 'add key' and copy the contents of 'id\_rsa.pub' into the box entitled key. You can set the label to whatever you like. Press save and you should get an email conformation that the key has been saved. Now you should be able to push to bitbucket.
