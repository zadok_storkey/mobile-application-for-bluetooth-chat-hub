## Planning the Project Using JIRA

### An Introduction to JIRA

#### The roadmap

![The JIRA Roadmap Tab](./../Images/JIRARoadmap.PNG)

The roadmap is basically like a gantt chart. It has the different long term things that need to be done with the app (referred to as 'Epics'). And the expected start and finish times, as well as the progress through them. As of writing this, they have been set it up with some times listed as start and end times without too much thought. If you think you can give a better estimate of when things will start and finish, which you probably can, then feel free to change the start times of each epic or add and remove epics. You could also just discuss whether you think the listed times are right in the comments of the epics.

#### The board

![The JIRA Board Tab](./../Images/JIRABoard.PNG)

The board is a list of tasks with columns showing how completed the task is. You can change how you view things by clicking the 'group by' dropdown on the top right. I think the most useful one to have it on is grouped by epic.

You can open a task by clicking on it and you can add a task by clicking the plus icon at the top of a column. You can also move task's columns by dragging.

Once you open a task it will have a screen where you can set some of the settings of the task. As well as assign people to it, view subtasks, and comment on the task. There is also a button to create a bitbucket branch for that task.

### What sort of things should I do?

There are a number of things you could do without any programming. You could add more tasks that need to be done. You could comment on tasks with suggestions for how to do them or reasons why they might need changing. You could discuss which tasks should be done first.

If you do want to modify the code you will still need to do things in JIRA. You should assign tasks to yourself and move them into 'in-progress' so other people can change them and then mark them as 'done' when they are done.