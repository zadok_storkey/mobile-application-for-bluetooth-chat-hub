## Making changes to the code

### An introduction to git

Git is a version control system usually used for programming. It is designed to cope with some of the challenges faced in software development such as multiple people working on code at once, the need to easily undo mistakes, and the possibility of publishing or running code which is different to the state of the code currently being worked on.

For more explaination as to what git does and why git is useful, I reccomend [this introduction to git](https://www.atlassian.com/git/tutorials/what-is-git). For an explaination to how you use git, I reccomend [this blog page](https://nvie.com/posts/a-successful-git-branching-model/).

### An introduction to bitbucket

Bitbucket is an online tool which manages a project using git. It hosts the central version of the repository which the local versions will pull things from and push them to. It also has an online interface for managing and making changes to the git project online. I will go over what a few of the pages do now.

#### Source

![The Bitbucket Source Tab](./../Images/BitbucketSource.PNG)

This is the page where you can view the actual contents of the repository. It has a file browser where you can navigate folders, open files and even edit them. It also has a dropdown at the top where you can choose what branch you are looking at the files in.

#### Commits

![The Bitbucket Commits Tab](./../Images/BitbucketCommits.PNG)

This is the page where the commits are tracked. Each dot is a commit with a link to a more in depth summary of that commit. The lines between the commits in different colours are the different branches they are on. You can see the date and commit messages to the right. You can also filter the commits by which branch they are on.

#### Branches

![The Bitbucket Branches Tab](./../Images/BitbucketBranches.PNG)

This is the page where the pull branches are listed. You can create a branch using the top right button.

#### Pull requests

![The Bitbucket Pull Request Tab](./../Images/BitbucketPullRequests.PNG)

This is the page where the pull requests are listed. You can also make a pull request by selecting the button in the top right corner.

### Before you start making changes

![Before Doing A Task](./../Images/JIRACheckCanDoTask.PNG)

Before you start work on a task you should make sure that:

- The task is in the 'To-do' column
- Nobody else has the task assigned to them
- There is no other task 'blocking' the task (it will tell you if there is)

If that is the case then you can assign the task to you and mark the task as 'in-progress' so that other people know you are working on it.

### Making changes using bitbucket and a web browser

It is possible to make changes using only a web browser, it is just generally less convenient than using an IDE like Visual Studio.

#### Small changes

For very small changes (e.g. editing some values in a single file), all you need to do is go on bitbucket and go to 'Source' on the menu on the left and you should have a page that looks a bit like a file browser. When you have got to that page, go to the dropdown at the top where it says master and select the branch you want to make the change to (most likely develop).

![Switching to develop](./../Images/BitbucketChangeBranchDevelop.PNG)

Now you are looking at the current state of that branch.

![The source page at develop](./../Images/BitbucketSourceDevelop.PNG)

From here, navigate the file browser until you find the file you want to change, and then click on it. This should bring you to a screen where you can make changes to the file.

![Editing a file on bitbucket](./../Images/BitbucketEditFile.PNG)

Once those changes have been made, you can commit them by pressing the commit button. This will bring up a popup asking for a commit message and whether you want to make a pull request. Fill the commit message in with an explaination of what you did. The pull request tickbox determines whether the change will be made straight away or whether I will need to approve it. If you are confident your change wont break anything, you don't need to tick the box. Otherwise, tick the box. It will then take you to another page to confirm you have made the changes.

![Committing a file on bitbucket](./../Images/BitbucketCommitFile.PNG)

Finally go onto JIRA, find the task you were working on and explain what you did, marking the task as done if you didn't tick the pull request box.

#### Larger changes

For larger changes where you are going to be working in the browser, you should be making a branch for the task you are working on. Go into JIRA and find the task you are working on, and click on it. Here you should find some blue text saying 'create-branch'.

![Creating a branch for a JIRA task](./../Images/JIRACreateTaskBranch.PNG)

This will lead you to a screen on bitbucket where you can create a new branch for that task. Fill in the form setting the branch to branch from to the branch you want to branch from (most likely develop) and setting the type of branch to the most appropriate option (most likely 'feature' or 'bugfix'). Leave the name alone and confirm you want to create a new branch.

![Bitbucket branch creation page](./../Images/BitbucketCreateTaskBranch.PNG)

Now you can make commits to this branch without the code interfering with other changes to the branch you came from. To do this, do the steps you took in the smaller changes section above, but go to your newly created branch and make the changes to files there. Keep changing files until you have made all the changes you needed to.

Once you are done you should create a pull request, as you have probably made lots of changes without testing them. Go to the pull requests tab on the left menu in bitbucket and select 'create pull request' in the top right corner.

![Bitbucket pull requests screen](./../Images/BitbucketPullRequestsCreateCircled.PNG)

On the left side, find the branch you have been working on, and on the right side, find the branch you branched it from and now want to merge it back into. Enter all the descriptions and press 'Create pull request'.

![Bitbucket pull request creation screen](./../Images/BitbucketCreatePullRequest.PNG)

Then as before, go on JIRA and mark the task as done.

### How to make changes using visual studio

This is more convenient but requires some setup first. You will need to have a proper setup with git and visual studio community 2019. The instructions for setting up git and visual studio can be found [here](./Installing%20Git%20And%20Visual%20Studio%20And%20Setting%20Up%20A%20Workspace.md).

#### Small changes

If you are only committing a few things, you probably wont need to create a branch. I would begin by opening git in the directory where your local version of the project is, and then typing `git checkout [the branch you are working in]`. This makes git show you the correct branch and it means you will be changing that branch. The next thing you need to do is type `git pull`. This syncs your local version of the project with the version in bitbucket. Now make whatever changes you need to make and save them.

![Git Checkout followed by Git Pull](./../Images/GitCheckoutGitPull.PNG)

At this point the changes exist in the filesystem but are not known to git. You can type `git add [file path]` to add changes to a file to git. You can also type `git add .` to add all the changes you have made at once. Once you have done that, you can type `git commit -m "[The description of the changes you have made]"`. This will store all the changes you have made in a commit.

![Git Add followed by Git Commit](./../Images/GitAddGitCommit.PNG)

Now that you have stored the changes locally you need to push the changes to bitbucket so everyone else can get them. To do this type `git push`. You won't need to do a pull request since the code should have already been tested by you. At this point you can check bitbucket to see that the changes have been uploaded.

![Git Push](./../Images/GitPush.PNG)

Finally mark whatever task you were working on as done.

#### Larger changes

For larger changes it is similar to smaller changes but you will want to create a new branch for the task, so find the task in JIRA and select create branch and fill in the branch to split from and the branch type.

![Creating a branch for a JIRA task](./../Images/JIRACreateTaskBranch.PNG)

Once you have created the branch you can run `git pull` on any branch in git to make your version of the project sync with the bitbucket project and reallise there is a new branch. Then you have to use `git checkout [branch you just made]`.

![Git Pull when there is a new branch](./../Images/GitPullDiscoverBranch.PNG)

Now make whatever changes you want and every significant step in solving the problem use `git add [file path]`/`git add .` and `git commit -m "[The description of the changes you have made]"`. You can also use `git push` to keep pushing your changes to bitbucket.

Once you have made your changes you need to merge your branch with the branch you originally created it from. Firstly go to that original branch with `git checkout [original branch]`. Then use `git pull` to sync any changes made to that original branch whilst you were working on the branch you made. 

Then use `git merge --no-ff [branch you made]` to merge the branch you made into the original branch. If all goes well it will bring you to a screen with a message on it saying you merged the branches and you just need to type `:qs` to save and exit with the message.

![Git Merge](./../Images/GitMerge.PNG)

Once you have merged all you need to do is git push on the original branch and the merge will be pushed back to bitbucket. Finally go to JIRA and mark the task you were working on as resolved.

#### Common problems

**(on a git commit) `nothing to commit, working directory clean`**

>You need to add your changes before committing.

**(on a git push) `Everything is up-to-date`**

>You need to commit your changes before pushing.

**`fatal: not a git repository (or any of the parent directories): .git`**

>You aren't in the right folder. Use `cd [full directory of your git folder]` to get there and then try again.

**`error: Your local changes to the following files would be overwritten by checkout:`**

>You are trying to switch branches when you have changed files and not committed them.

>If you want to keep the changes, you should commit them by using `git add .` and `git commit -m [The description of the changes you have made]`.

>If you want to discard the changes and reset to the most recent commit, you should type `git reset --hard`. To be clear this will remove all uncommitted files.

**(in red) `error: failed to push some refs to '[location of bitbucket repository]'`**
**(in yellow)`hint: Updates were rejected because the tip of your current branch is behind`**

>This means that someone has made changes to the branch you were working on whilst you were working on it locally. You need to use `git pull` to get the changes they made and merge them with the changes you made. Then `git push` again. Please do not use `git push -f` or `git push --force`, even if you see advice on the internet telling you to do so, because it will break things.

**`Automatic merge failed; fix conflicts and then commit the result.`**

>This means that both edits you are trying to merge made a modification to the same part of the same file. In the file it will have some pointers showing the two versions of the conflicting parts. You need to manually modify that file to work out what it should look like and then use `git add [file path]`/`git add .` and then `git commit` to finish the merge yourself.

**I'm stuck in a text editor which I can't exit**

>The text editor is vim. If you want to type then pressing escape and then i will allow you to write text. If you want to exit then press escape and the type `:q` (if it is expecting you to save you might need to type `:!q` to force quit). If you want to save and exit type `:wq`. Look it up if you want to understand how to use it in more detail.

**I messed up and I need to reverse the changes I made**

>There are a number of different ways to reverse errors in git depending what you did.

>For things which have not committed, `git reset --hard` will reset **All** the changes that have been made since the last commit.

>For things which have been committed and **have not been pushed**, you should use `git reset --hard Head~1` to remove the changes in the previous commit. You can change the number at the end to the number of commits you want to go back, but I reccomend doing it one commit at a time to be safe. If you need to see the history of commits you can use `git log`.

>For things which have been committed and pushed. You may see advice online to use `git push -f` or `git push --force`. **Do not do that**. Instead use `git revert HEAD` (note that `HEAD` is not the same as `HEAD~1`). This will make a new commit which reverses the most recent commit. If you want to reverse a different commit than the most recent one, you can use `git revert [commit id]` where the id is the set of hexidecimal digits shown in `git log` (you don't have to enter the whole id, just the first 5 or 6 characters). Once you have created the revert commits you can push those to bitbucket with `git push`.