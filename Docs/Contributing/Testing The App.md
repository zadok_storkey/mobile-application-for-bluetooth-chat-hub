## Testing the app

If you want to test the application on your own device there are a couple of ways to do it. Unfortunately you can only get the android copy on your device because apple have lots of restrictions which make it difficult to add apps you have made to your device.

### Downloading the app onto your android phone

If you are using an android phone then you will be able to download the apk for each version on the bitbucket [downloads](https://bitbucket.org/zadok_storkey/mobile-application-for-bluetooth-chat-hub/downloads/) page.

### Using visual studio to put the app on to your android phone

If you have got a local copy of the project (see [Installing Visual Studio and setting up a workspace](./Installing%20Git%20And%20Visual%20Studio%20And%20Setting%20Up%20A%20Workspace.md)), you can upload the app onto your android phone with little difficulty. Firstly, you need to turn on developer mode (how you do this varies from device to device so you will have to google it). After you have done that then you need to turn usb debugging on and use a usb connector to plug your phone into the usb port on your computer. Then open visual studio and rather than running in an emulator the app should run on your phone. After you are done debugging, the most recently added version will stay until you uninstall it.