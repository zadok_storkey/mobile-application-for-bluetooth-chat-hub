## NameAndColorSelectPageViewModel

The NameAndColorSelectPageViewModel is a ViewModel containing the logic and data for the [NameAndColorSelectPage](./NameAndColorSelectPage.md) and its [ColorPickerPopupPage](./ColorPickerPopupPage.md). It extends `ViewModelBase`, a class from the [MVVMLight](https://github.com/lbugnion/mvvmlight) package, and implements the interface `INameAndColorSelectPageViewModel`, which just has all the public methods and properties for the class.

The NameAndColorSelectPageViewModel contains two public properties, `UsernameEntry` and `Color`. Both contain getters and setters which modify private properties, `_usernameEntry` and `_color` and call `RaisePropertyChanged` (a method of `ViewModelBase`) in the setter. `UsernameEntry` stores the user's chosen username and `Color` stores the user's chosen colour.

When the user presses the join button on the [NameAndColorSelectPage](./NameAndColorSelectPage.md), `OnJoinButtonPressed` is called. This simply changes the current page in the [ModifiedNavigationPage](./ModifiedNavigationPage.md) to [MessagingPage](./MessagingPage.md).

When the user presses the colour button on the [NameAndColorSelectPage](./NameAndColorSelectPage.md), `OnColorPopupButtonPressed` is called. This creates a [ColorPickerPopupPage](./ColorPickerPopupPage.md) over this page using the `PopupNavigation` class of the [Rp.Plugins.Popup](https://github.com/rotorgames/Rg.Plugins.Popup) package. Most of this function is spent calculating the position to place the [ColorPickerPopupPage](./ColorPickerPopupPage.md) at.

There is also an `OnColorButtonPressed` method which is called when a color is selected on the [ColorPickerPopupPage](./ColorPickerPopupPage.md). This updates the color on the [NameAndColorSelectPage](NameAndColorSelectPage.md) and closes the [ColorPickerPopupPage](./ColorPickerPopupPage.md).

### Code

![NameAndColorSelectPageViewModel.cs](./../Images/Code/0.2.0/Annotated/NameAndColorSelectPageViewModel_Annotated_0.2.0.png)

![NameAndColorSelectPageViewModel.cs](./../Images/Code/0.2.0/Annotated/INameAndColorSelectPageViewModel_Annotated_0.2.0.png)