## DeviceSelectPageViewModel

The DeviceSelectPageViewModel is a ViewModel containing the logic and data for [DeviceSelectPage](./DeviceSelectPage.md). It extends `ViewModelBase`, a class from the [MVVMLight](https://github.com/lbugnion/mvvmlight) package, and implements the interface `IDeviceSelectPageViewModel`, which just has all the public methods and properties for the class.

The DeviceSelectPageViewModel has a single public property that has a getter, `AvailableDevices`. `AvailableDevices` contains a list of known devices to connect to. Currently the getter just returns a placeholder list of devices as we haven't yet added the functionality for looking for devices.

The DeviceSelectPageViewModel has two methods, `OnDeviceSelected` and `OnDevicePressed`. `OnDeviceSelected` is called when a list item is clicked on when it isn't selected. This will be used for connecting but currently isn't being used for anything. `OnDevicePressed` is called when an item is pressed, this is used to change the page in the [ModifiedNavigationPage](./ModifiedNavigationPage.md) to [NameAndColorSelectPage](./NameAndColorSelectPage.md). In the future it will only allow connecting if the device is connected to.

### Code

![DeviceSelectPageViewModel.cs](./../Images/Code/0.2.0/Annotated/DeviceSelectPageViewModel_Annotated_0.2.0.png)

![IDeviceSelectPageViewModel.cs](./../Images/Code/0.2.0/Annotated/IDeviceSelectPageViewModel_Annotated_0.2.0.png)