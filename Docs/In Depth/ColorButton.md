## ColorButton

The ColorButton class is a custom subclass of [Button](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/button) used in the [ColorPickerPopupPage](./ColorPickerPopupPage.md). It contains the code to create a `ColorToSelect` property which can be accessed both in xaml and c#. It also contains a constructor which sets some up some of the appearance: [WidthRequest](https://docs.microsoft.com/en-us/dotnet/api/xamarin.forms.visualelement.widthrequest?view=xamarin-forms), [HeightRequest](https://docs.microsoft.com/en-us/dotnet/api/xamarin.forms.visualelement.heightrequest?view=xamarin-forms) and [CornerRadius](https://docs.microsoft.com/en-us/dotnet/api/xamarin.forms.button.cornerradius?view=xamarin-forms#Xamarin_Forms_Button_CornerRadius). It also binds its `Pressed` property to the [NameAndColorSelectViewModel](./NameAndColorSelectViewModel.md)'s `OnColorButtonPressed`.

### Code

![ColorButton.cs](./../Images/Code/0.2.0/Annotated/ColorButton_Annotated_0.2.0.png)