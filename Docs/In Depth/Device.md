## Device

A device is a class reperesenting a device to connect to. It currently contains just a `DeviceName`. It uses public propertes with only getters so that they can be bound to but are also readonly. Eventually it will have some more information about the device.

### Code

![Device.cs](./../Images/Code/0.2.0/Annotated/Device_Annotated_0.2.0.png)