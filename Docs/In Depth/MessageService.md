## MessageService

The MessageService manages the storage and retrieval of [messages](./Message.md) from and to the [MessageHolder](./MessageHolder.md).

When it is instantiated the MessageService creates a private variable `_messageHolder` containing a new [MessageHolder](./MessageHolder.md) which it will use to hold the [messages](./Message.md).

The MessageService also has a `Messages` property which just returns `_messageHolder.Messages`.

Finally the MessageService has an `AddMessage` method which takes a string and uses that to create a new [message](./Message.md) in `_messageHolder`. It also calls its own `OnNewMessageEvent` to tell the [MessagingPageViewModel](./MessagingPageViewModel.ms) to update its messages to match the MessageService's public property `Messages`.

### Code

![MessageService.cs](./../Images/Code/0.2.0/Annotated/MessageService_Annotated_0.2.0.png)

![IMessageService.cs](./../Images/Code/0.2.0/Annotated/IMessageService_Annotated_0.2.0.png)