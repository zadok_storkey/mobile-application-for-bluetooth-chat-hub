## MessagingPageViewModel

The MessagingPageViewModel is a ViewModel containing the logic and data for [MainPage](./MainPage.md). It extends `ViewModelBase`, a class from the [MVVMLight](https://github.com/lbugnion/mvvmlight) package, and implements the interface `IMessagingPageViewModel`, which just has all the public methods and properties for the class.

The MessagingPageViewModel has two public properties, `Messages` and `EntryText`, which are for the [MainPage](./MainPage.md) to bind to. `Messages` doesn't have a setter but `EntryText` does, as it can also be modified from the view. The setter on `EntryText` calls `RaisePropertyChanged` (a method of ViewModelBase) so that the PageView knows that it has changed.

When the MessagingPageViewModel is constructed it finds the [MessageService](./MessageService.md) and attaches the `UpdateMessage` method to the `OnNewMessage` event, so that when there is a new message it can update `_messages` and tell the [MainPage](./MainPage.md) that `Messages` has changed.

Finally it has an `OnMessageEntryCompleted` event which is for the entry in the [MainPage](./MainPage.md) to call when it is done. It resolves the entry of messages by passing the message on to the [MessageService](./MessageService.md) and clearing `_entryText`.

### Code

![MessagingPageViewModel.cs](./../Images/Code/0.2.0/Annotated/MessagingPageViewModel_Annotated_0.2.0.png)

![IMessagingPageViewModel.cs](./../Images/Code/0.2.0/Annotated/IMessagingPageViewModel_Annotated_0.2.0.png)