## ValueMinusFiftyConverter

The ValueMinusFiftyConverter is a [converter](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/data-binding/converters) which converts and integer to an integer fifty less than it. It was originally to make a message fifty less than the width of the [CollectionView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/collectionview/) in [MainPage](MainPage.md) but is no longer being used for that.

### Code

![ValueMinusFiftyConverter.cs](./../Images/Code/0.2.0/Annotated/ValueMinusFiftyConverter_Annotated_0.2.0.png)