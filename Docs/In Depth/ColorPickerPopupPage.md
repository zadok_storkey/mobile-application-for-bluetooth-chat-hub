## ColorPickerPopupPage

The ColorPickerPopupPage is the popup (popups are special pages added in the [Rg.Plugins.Popup](https://github.com/rotorgames/Rg.Plugins.Popup) plugin) which is created when the [NameAndColorSelectPage](./NameAndColorSelectPage.md) needs the user to select a color. It has a lot of formatting to get it to the right size and position and then it just consists of a grid of [ColorButtons](./ColorButton.md).

The constructor of the code behind the ColorPickerPopupPage takes a position and then makes the popup come out of that position. It also has a lot of random functions available to deal with animations but they are currently not doing anything.

### Code

![ColorPickerPopupPage.xaml](./../Images/Code/0.2.0/Annotated/ColorPickerPopupPage_Annotated_XAML_0.2.0.png)

![ColorPickerPopupPage.xaml.cs](./../Images/Code/0.2.0/Annotated/ColorPickerPopupPage_Annotated_CS_0.2.0.png)