## Chat Entry Renderer

This code modifies how android renders [Entries](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/text/entry) so that there is no bar below the entry in the [MainPage](./MainPage.md). It was taken from an answer from stackoverflow so I have credited the page at the top after doing some research to ensure that it was licensed in a way which meant I can use it.

### Code

![ChatEntryRenderer.cs](./../Images/Code/0.2.0/Annotated/ChatEntryRenderer_Annotated_0.2.0.png)