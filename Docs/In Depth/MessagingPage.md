## MessagingPage

The MessagingPage is the most important page in the project. It is the interface for the sending and recieving messages to the other people. It is connected to the [MessagingPageViewModel](./MessagingPageViewModel.md) so can get its data from there.

The MessagingPage begins with a [StackLayout](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/stack-layout) so that it can have the messages followed by the [Entry](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/text/entry). The messages are a [CollectionView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/collectionview/) where the content is bound to [MessagingPageViewModel.Messages](./MessagingPageViewModel.md). That means that each of the messages will be created as a copy of the template.

The template of the message is a [ContentView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/contentview) containing a [Frame](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/frame) containing a [Label](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/text/label). The [Frame](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/frame) is there to get the rounded effect over the messages and the [Label](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/text/label) is to create the actual text.

Finally the entry area just consists of a [ContentView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/contentview) containing a [Frame](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/frame) for rounding which contains an [Entry](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/text/entry), with its `Text` bound to [MessagingPageViewModel.EntryText](./MessagingPageViewModel.md).

The structure of the page itself is fairly simple, the most complicated part of this page is the formatting of the views that make up the page. There are a number of different methods for aligning and positioning the views inside the page. For example it uses [margins and padding](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/margin-and-padding) to make the elements fit their containers well and uses a combination of [WidthRequests](https://docs.microsoft.com/en-us/dotnet/api/xamarin.forms.visualelement.widthrequest?view=xamarin-forms) and [LayoutOptions](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/layout-options) to add the gaps to the left or right of the messages.

### Code

![MessagingPage.xaml](./../Images/Code/0.2.0/Annotated/MessagingPage_Annotated_XAML_0.2.0.png)

![MessagingPage.xaml.cs](./../Images/Code/0.2.0/Annotated/MessagingPage_Annotated_CS_0.2.0.png)