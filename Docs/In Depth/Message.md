## Message

Message is a class containing the `MessageText`, the `TextColor` and the `BackgroundColor` of a message as well as whether or not the message is sent by the user. It uses public propertes with only getters so that they can be bound to but are also readonly.

### Code

![Message.cs](./../Images/Code/0.2.0/Annotated/Message_Annotated_0.2.0.png)