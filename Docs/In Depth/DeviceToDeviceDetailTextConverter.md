## DeviceToDeviceDetailTextConverter

The DeviceToDeviceDetailTextConverter is a [converter](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/data-binding/converters) which is used by the [DeviceSelectPage](./DeviceSelectPage.md) to turn a [Device](./Device.md) into some text describing the connection state of the device.

Currently it just returns some placeholder text because we haven't started dealing with connection yet.

### Code

![DeviceToDeviceDetailTextConverter.cs](./../Images/Code/0.2.0/Annotated/DeviceToDeviceDetailTextConverter_Annotated_0.2.0.png)