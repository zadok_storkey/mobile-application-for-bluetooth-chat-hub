## MessageHolder

The MessageHolder is a class containing an `ObservableCollection` of [Messages](./Message.md). It has a `Messages` property which has a getter and no setter and returns the private `_messages` property. It also has an `AddMessages` method which adds a [Message](./Message.md) to the collection.

### Code

![MessageHolder.cs](./../Images/Code/0.2.0/Annotated/MessageHolder_Annotated_0.2.0.png)