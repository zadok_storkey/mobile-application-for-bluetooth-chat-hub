## IocLocator

The IocLocator creates and supplies instances of the viewmodels and services with the help of the `SimpleIoc` class added by [MVVMLight](https://github.com/lbugnion/mvvmlight). IocLocator is a static class so there cannot be instances of it. When it is constructed it tells `SimpleIoc` to register an instance of each of the viewmodel and service interfaces and tells `SimpleIoc` what the default implementations should be.

When another part of the code requests the variables reperesenting the viewmodels or services it actually just looks up the values in `SimpleIoc`.

It currently contains an instance of [MessagingService](./MessagingService.md), [MessagingPageViewModel](./MessagingPageViewModel.md), [DeviceSelectPageViewModel](./DeviceSelectPageViewModel.md) and [NameAndColorSelectPageViewModel](./NameAndColorSelectPageViewModel.md).

### Code

![IocLocator.cs](./../Images/Code/0.2.0/Annotated/IocLocator_Annotated_0.2.0.png)