## BooleanToLayoutOptionConverter

The BooleanToLayoutOptionConverter is a [converter](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/data-binding/converters) which converts from a boolean to either `LayoutOptions.End` if the value is true or `LayoutOptions.Start` if the value is false. It is used by [MainPage](./MainPage.md) to convert whether a message was sent to the user to the xamarin [LayoutOptions](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/layout-options) which determine whether the message should be on the right or the left of the screen.

### Code

![BooleanToLayoutOptionConverter.cs](./../Images/Code/0.2.0/Annotated/BooleanToLayoutOptionConverter_Annotated_0.2.0.png)