## ModifiedNavigationPage

ModifiedNavigationPage is a custom page which extends [NavigationPage](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/navigation/hierarchical) with the expectation of only having two pages on the stack at a time. It adds a method for setting the current second page to a page of your choice by adding your page to the top of the stack and then removing the second page in the stack so at no point do you see the first page.

### Code

![ModifiedNavigationPage.xaml](./../Images/Code/0.2.0/Annotated/ModifiedNavigationPage_Annotated_0.2.0.png)