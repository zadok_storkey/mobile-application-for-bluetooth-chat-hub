## DeviceSelectPage

The DeviceSelectPage is the page where the user chooses the device to connect to. It consists of a list of devices where the user can press on one to select it and advance to the next screen, the [NameAndColorSelectScreen](./NameAndColorSelectScreen.md).

The DeviceSelectPage extends [ContentPage](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/custom-renderer/contentpage) and the outermost content is a [ListView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/listview/). This [ListView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/listview/) is then bound to `AvailableDevices` (the [BindingContext](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/xaml/xaml-basics/data-binding-basics) for this page is the [DeviceSelectViewModel](./DeviceSelectViewModel.md) supplied by the [IocLocator](./IocLocator.md)). Other than the [ListView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/listview/) contents there isn't anything else on the page, making this a fairly simple page.

The template for the [ListView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/listview/) items is just a [TextCell](https://docs.microsoft.com/en-us/dotnet/api/xamarin.forms.textcell) (the simplest thing which can go in a [ListView](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/listview/)) with the `Text` bound to the [Device](./Device.md)'s `DeviceName`, and the `Detail` bound to the [Device](./Device.md) itself, with a [DeviceToDetailTextConverter](./DeviceToDetailTextConverter) to turn the [Device](./Device.md) into connection information.

### Code

![DeviceSelectPage.xaml](./../Images/Code/0.2.0/Annotated/DeviceSelectPage_Annotated_XAML_0.2.0.png)

![DeviceSelectPage.xaml.cs](./../Images/Code/0.2.0/Annotated/DeviceSelectPage_Annotated_CS_0.2.0.png)